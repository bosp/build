ifndef CONFIG_ARM_TOOLCHAIN
ARM_TOOLCHAIN := linaro
else
ARM_TOOLCHAIN := $(subst ",,$(CONFIG_ARM_TOOLCHAIN))
endif
CTNG_CONFIG=ct-ng-config-linux

include toolchains/arm/$(ARM_TOOLCHAIN).mk

# Configure GCC compiler version
GCC_VERSION := $(shell $(CXX) -dumpversion | cut -f1,2,3 -d.)
GCC_MAJOR := $(shell $(CXX) -dumpversion | cut -f1 -d.)
GCC_MINOR := $(shell $(CXX) -dumpversion | cut -f2 -d.)
GCC_TAG   := $(GCC_MAJOR)$(GCC_MINOR)

TARGET_FLAGS := --sysroot=${PLATFORM_SYSROOT} -march=armv7-a -mfpu=neon

ifdef CONFIG_TARGET_ARM_CORTEX_A15
TARGET_FLAGS += -mtune=cortex-a15
endif

ifdef CONFIG_TARGET_ARM_CORTEX_A9
TARGET_FLAGS += -mtune=cortex-a9
endif

