ifndef ARM_TOOLCHAIN
ARM_TOOLCHAIN := bionic
endif
CTNG_CONFIG=ct-ng-config-android

include toolchains/arm/$(ARM_TOOLCHAIN).mk

# Configure GCC compiler version
GCC_VERSION := $(shell $(CXX) -dumpversion | cut -f1,2,3 -d.)
GCC_MAJOR := $(shell $(CXX) -dumpversion | cut -f1 -d.)
GCC_MINOR := $(shell $(CXX) -dumpversion | cut -f2 -d.)
GCC_TAG   := $(GCC_MAJOR)$(GCC_MINOR)

TARGET_FLAGS := --sysroot=${PLATFORM_SYSROOT} -fPIC
TARGET_EXE_LDFLAGS := -pie


LOCAL_EXPORT_LDLIBS += -latomic
