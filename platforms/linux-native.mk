.PHONY:
ifndef GCC_VERSION

$(info )
$(info ========== Checking GCC version ===========)

GCC_VERSION := $(shell gcc -dumpversion | cut -f1,2,3 -d.)

$(info [linux-native.mk] GCC version : $(GCC_VERSION))

CPP := $(shell readlink -f `which cpp`)
CXX := $(shell readlink -f `which g++`)
CC  := $(shell readlink -f `which gcc`)

$(info [linux-native.mk] CPP path : $(CPP))
$(info [linux-native.mk] CXX path : $(CXX))
$(info [linux-native.mk] CC  path : $(CC))


endif #ifndef GCC_VERSION

# Configure GCC compiler version
GCC_MAJOR := $(shell gcc -dumpversion | cut -f1 -d.)
GCC_MINOR := $(shell gcc -dumpversion | cut -f2 -d.)
GCC_TAG   := $(GCC_MAJOR)$(GCC_MINOR)

TARGET_FLAGS := -march=native
ifdef CONFIG_TARGET_LINUX_NATIVE_I386
TARGET_FLAGS := -march=native -m32
endif

THERMAL_SENSORS_DISCOVERY = $(BASE_DIR)/barbeque/tools/bbqueDiscoverTSensors.sh
CONFIGPATH_PM=$(BASE_DIR)/build/platforms

PLATFORM_HAS_PARSEC := 1
platform_check:
platform_setup:
	@echo "============= Linux Platform Setup ================="
	@cd $(CONFIGPATH_PM) && $(THERMAL_SENSORS_DISCOVERY)
	@echo "===================================================="
platform_distclean:
