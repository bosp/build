
# Compilation configuration

# Check if the user has required specific -j option
MAKE_PID := $(shell echo $$PPID)
JOB_FLAG := $(filter -j%, $(subst -j ,-j,$(shell ps T | grep "^\s*$(MAKE_PID).*$(MAKE)")))
CPUS     := $(subst -j,,$(JOB_FLAG))

# Otherwise let's take the core number of the machine.
CORES := $(shell grep processor /proc/cpuinfo | wc -l)
CPUS  += $(shell echo "$(CORES)" | bc)
CPUS  := $(word 1, $(CPUS))
MACH  := $(shell uname -m)

# CMake configuration
ifeq ($(CONFIG_BOSP_RUNTIME_PATH), "")
CONFIG_BOSP_RUNTIME_PATH=$(BASE_DIR)/out
endif

ifeq ($(CONFIG_BOSP_RUNTIME_RWPATH), "")
CONFIG_BOSP_RUNTIME_RWPATH=$(BASE_DIR)/out/var/bbque
endif

BOSP_SYSROOT=$(CONFIG_BOSP_RUNTIME_PATH)
BOSP_CMAKE_MODULES := "$(BOSP_SYSROOT)/usr/share/bbque/.cmake"
CMAKE_COMMON_OPTIONS = \
		-DCMAKE_CXX_COMPILER=$(CXX)                      \
		-DCMAKE_CXX_FLAGS="$(TARGET_FLAGS)"              \
		-DCMAKE_C_COMPILER=$(CC)                         \
		-DCMAKE_C_FLAGS="$(TARGET_FLAGS)"                \
		-DCMAKE_EXE_LINKER_FLAGS="$(TARGET_EXE_LDFLAGS)" \
		-DCMAKE_BUILD_TYPE=$(BUILD_TYPE)                 \
		-DBOSP_CMAKE_MODULES=$(BOSP_CMAKE_MODULES)       \
		-DGCC_VERSION=\"$(GCC_MAJOR).$(GCC_MINOR)\"      \
		-DGCC_TAG=$(GCC_TAG)                             \
		-DCONFIGPATH=$(KCONFIG_OUTPUT_CMAKE)             \
		-DBASE_DIR=$(BASE_DIR)                           \
		-DCMAKE_INSTALL_PREFIX=$(BOSP_SYSROOT)


verbose_parallelism:
	@echo
	@echo "==== Compilation parallelism ===="
	@echo "Compiling with $(CPUS) parallel jobs."
	@echo

setup_build: $(BOSP_SYSROOT)/usr/bin
$(BOSP_SYSROOT)/usr/bin:
	@echo
	@echo "==== Setup build directory [$(BOSP_SYSROOT)] ===="
	@mkdir -p \
		$(BOSP_SYSROOT)/etc/bbque/recipes \
		$(BOSP_SYSROOT)/etc/default \
		$(BOSP_SYSROOT)/sbin \
		$(BOSP_SYSROOT)/usr/bin \
		$(BOSP_SYSROOT)/usr/sbin \
		$(BOSP_SYSROOT)/usr/share/bbque/.cmake \
		$(BOSP_SYSROOT)/usr/python \
		$(BOSP_SYSROOT)/var/lock \
		$(BOSP_SYSROOT)/var/run \
		$(BOSP_SYSROOT)/var/bbque/ocl \
		>/dev/null 2>&1
	@echo "[envsetup.mk] Build dir: "$(BOSP_SYSROOT)
	@echo "[envsetup.mk] Build CPUs: "$(CPUS)
	@echo

# This check is required only for native compilation
# In the other cases a proper cross-compiler is produced by this build system.
ifdef CONFIG_TARGET_NATIVE_COMPILATION

# This should define the minimum required version, _minus_ 1 for the patch level
GCC_MIN_VER := 4.7
GCC_CUR_VER := $(shell $(CXX) -dumpversion)

# Build numerical version of required and current GCC version
# This addresses the possibility of double-digit numbers in any of the version
# part, and possibility of missing 3-rd part of the version in output of gcc
# -dumpversion (which is the case in some earlier gcc versions).
# Ref: http://stackoverflow.com/a/17947005
GCC_MIN_VER_NR := $(shell expr \
	`echo "$(GCC_MIN_VER)" |   \
		sed -e 's/\.\([0-9][0-9]\)/\1/g' -e 's/\.\([0-9]\)/0\1/g' -e 's/^[0-9]$$/&00/'`)
GCC_CUR_VER_NR := $(shell expr \
	`echo "$(GCC_CUR_VER)" |   \
		sed -e 's/\.\([0-9][0-9]\)/\1/g' -e 's/\.\([0-9]\)/0\1/g' -e 's/^[0-9]$$/&00/'`)

endif # CONFIG_TARGET_NATIVE_COMPILATION

setup_check: verbose_parallelism platform_check
	@echo
	@echo "==== Checking building system dependencies ==="
	@echo -n "[envsetup.mk] Checking for git.......... "
	@which git || (\
		echo "[envsetup.mk] MISSING: install [git-core,gitk,git-gui]" && \
		exit 1)
	@echo -n "[envsetup.mk] Checking for cmake........ "
	@which cmake || (echo "[envsetup.mk] MISSING: install [cmake]" && exit 1)
	@echo -n "[envsetup.mk] Checking for configure.... "
	@which autoconf || (echo "[envsetup.mk] MISSING: install [autoconf]" && exit 1)
	@echo -n "[envsetup.mk] Checking for autoreconf... "
	@which autoreconf || (echo "[envsetup.mk] MISSING: install [autoconf]" && exit 1)
	@echo -n "[envsetup.mk] Checking for libtoolize... "
	@which libtoolize || (echo "[envsetup.mk] MISSING: install [libtool]" && exit 1)
	@echo -n "[envsetup.mk] Checking for make......... "
	@which make || (echo "[envsetup.mk] MISSING: install [automake]" && exit 1)
	@echo -n "[envsetup.mk] Checking for doxygen...... "
	@which doxygen || (\
		echo "[envsetup.mk] Missing 'doxygen': install [doxygen]" && \
		exit 1)
ifdef CONFIG_TARGET_NATIVE_COMPILATION
# Uncomment this and indent for numerical version check debugging
# @echo "[envsetup.mk] GCC min ver: $(GCC_MIN_VER) ($(GCC_MIN_VER_NR))"
# @echo "[envsetup.mk] GCC cur ver: $(GCC_CUR_VER) ($(GCC_CUR_VER_NR))"
	@echo -n "[envsetup.mk] Checking for gcc.......... "
	@if [ $(GCC_MIN_VER_NR) -le $(GCC_CUR_VER_NR) ]; then \
		echo "$(GCC_CUR_VER) ($(CXX))"; \
	else \
		echo '[envsetup.mk] FAILED\n'; \
		echo "[envsetup.mk]    Current GCC version      : ${GCC_CUR_VER}      ($(CXX))"; \
		echo "[envsetup.mk]    Min required GCC version : ${GCC_MIN_VER}\n\n"; \
		exit 1; \
	fi

endif # CONFIG_TARGET_NATIVE_COMPILATION
	@echo
	@echo "[envsetup.mk] Check SUCCESS: all required tools are available."
	@echo

clean_out:
	@echo
	@echo "==== Clean-up build directory [$(BOSP_SYSROOT)] ===="
	@[ -d $(BASE_DIR)/out ] && \
		rm -rf $(BASE_DIR)/out 2>/dev/null || \
		exit 0


