
# Building system configuration
BASE_DIR := $(shell pwd)
BUILD_DIR ?= $(BASE_DIR)/out
BUILD_SYSTEM := $(TOPDIR)build/core

# Setup default target (required for cold-and-dark startup)
BUILD_TYPE := Release
CONFIG_TARGET_PLATFORM := linux-native

# KConfig configuration
KCONFIG_PATH := $(BASE_DIR)/external/tools/kconfig/
KCONFIG_FILENAME :=$(BASE_DIR)/build/configuration/Kconfig
KCONFIG_CONFIG := $(BASE_DIR)/build/configuration/bosp-config
KCONFIG_OUTPUT_CMAKE := $(BASE_DIR)/build/configuration/bosp-config.cmake

KCONFIG_TMP := $(BASE_DIR)/build/configuration/bosp-modules.tmp
KCONFIG_MOD := $(BASE_DIR)/build/configuration
KCONFIG_CMK := $(BASE_DIR)/build/core


include $(KCONFIG_PATH)/kconfig.inc.mk

.PHONY: boostrap
bootstrap:
	$(info )
	$(info ==== Bootstraping BOSP Building System ====)
	@find $(BASE_DIR) -name 'Kconfig' -o -name 'bosp.mk' \
		| awk '/Kconfig/{print "source \"" $$0 "\""} /bosp.mk/{print "-include " $$0}' \
		> $(KCONFIG_TMP)
	@echo 'Looking up for BOSP modules Configurations...'
	@echo
	@echo '.:: External REQUIRED modules:'
	@grep 'external/required/.*/Kconfig' $(KCONFIG_TMP) \
		> $(KCONFIG_MOD)/Kconfig-external-required
	@grep 'external/required/.*/bosp.mk' $(KCONFIG_TMP) \
		> $(KCONFIG_CMK)/external-required.mk
	@sed -e 's/\/Kconfig//' -e 's/source //' -e 's/"//g' \
		$(KCONFIG_MOD)/Kconfig-external-required
	@echo
	@echo '.:: External OPTIONAL modules:'
	@grep 'external/optional/.*/Kconfig' $(KCONFIG_TMP) \
		> $(KCONFIG_MOD)/Kconfig-external-optional
	@grep 'external/optional/.*/bosp.mk' $(KCONFIG_TMP) \
		> $(KCONFIG_CMK)/external-optional.mk
	@sed -e 's/\/Kconfig//' -e 's/source //' -e 's/"//g' \
		$(KCONFIG_MOD)/Kconfig-external-optional
	@echo
	@echo '.:: External TOOLS modules:'
	@grep 'external/tools/.*/Kconfig' $(KCONFIG_TMP) \
		> $(KCONFIG_MOD)/Kconfig-external-tools
	@grep 'external/tools/.*/bosp.mk' $(KCONFIG_TMP) \
		> $(KCONFIG_CMK)/external-tools.mk
	@sed -e 's/\/Kconfig//' -e 's/source //' -e 's/"//g' \
		$(KCONFIG_MOD)/Kconfig-external-tools
	@echo
	@echo '.:: Benchmarks modules:'
	@grep 'benchmarks/.*/Kconfig' $(KCONFIG_TMP) \
		> $(KCONFIG_MOD)/Kconfig-benchmarks-modules
	@grep 'benchmarks/.*/bosp.mk' $(KCONFIG_TMP) \
		> $(KCONFIG_CMK)/benchmarks-modules.mk
	@sed -e 's/\/Kconfig//' -e 's/source //' -e 's/"//g' \
		$(KCONFIG_MOD)/Kconfig-benchmarks-modules
	@echo
	@echo '.:: Contrib TESTING applications:'
	@grep 'contrib/testing/.*/Kconfig' $(KCONFIG_TMP) \
		> $(KCONFIG_MOD)/Kconfig-contrib-testing
	@grep 'contrib/testing/.*/bosp.mk' $(KCONFIG_TMP) \
		> $(KCONFIG_CMK)/contrib-testing.mk
	@sed -e 's/\/Kconfig//' -e 's/source //' -e 's/"//g' \
		$(KCONFIG_MOD)/Kconfig-contrib-testing
	@echo
	@echo '.:: Sample applications:'
	@grep -v 'ocl-samples' $(KCONFIG_TMP) | grep 'samples/Kconfig' \
		> $(KCONFIG_MOD)/Kconfig-samples
	@grep -v 'ocl-samples' $(KCONFIG_TMP) | grep 'samples/bosp.mk' \
		> $(KCONFIG_CMK)/samples.mk
	@sed -e 's/\/Kconfig//' -e 's/source //' -e 's/"//g' \
		$(KCONFIG_MOD)/Kconfig-samples
	@rm $(KCONFIG_TMP)

setup_config:
	$(info )
	$(info ==== Checking BOSP build configuration ====)
ifndef CONFIG_DONE
	$(warning [config.mk] ERROR: BOSP not yet configured for build \
		You can configure it with "make menuconfig" \
		or "make xconfig", at your choose.)
	$(error [config.mk] BOSP compilation aborted)
endif
ifndef CXX
	$(warning [config.mk] ERROR: required C/C++ compiler NOT found)
	$(error [config.mk] BOSP compilation aborted)
endif
ifdef CONFIG_TARGET_LINUX
	$(info [config.mk] BOSP configured for a Generic-Linux platform)
endif
ifdef CONFIG_TARGET_ANDROID
	$(info [config.mk] BOSP configured for a Android-Linux platform)
endif
ifdef CONFIG_TARGET_ARM
	$(info [config.mk] Cross-compilation for ARM)
endif
ifdef CONFIG_TARGET_ARNDALE_OCTA
	$(info [config.mk] Target: Arndale Octa, Exynos 5420, 4xCortex-A15 + 4xCortex-A7)
endif
ifdef CONFIG_TARGET_ODROID_XU
	$(info [config.mk] Target: Odroid-XU3, Exynos 5422, 4xCortex-A15 + 4xCortex-A7)
endif
ifdef CONFIG_TARGET_PANDABOARD
	$(info [config.mk] Target: Pandaboard, OMAP4, 2xCortex-A9)
endif
ifdef CONFIG_TARGET_FREESCALE_IMX6Q
	$(info [config.mk] Target: Freescale, iMX6Q, 4xCortex-A9)
endif

