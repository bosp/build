
      ____  ____  _____ ____
     / __ )/ __ \/ ___// __ \           )   )  ()
    / __  / / / /\__ \/ /_/ /           )   )   \
   / /_/ / /_/ /___/ / ____/          _____________
  /_____/\____//____/_/               \           /
                                       \  o   o  /
                                        \_______/
                                        /       \

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Welcome to the Barbeque Open Source Project (BOSP)
Version: 1.2 ("Emily Entrecote").

To properly download and install the BarbequeRTRM framework, please visit:

https://bosp.deib.polimi.it/


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The BOSP Development Team

